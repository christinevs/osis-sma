package com.osis.services;

import java.util.List;

import com.osis.models.Forum;

public interface ForumService {
	public Forum save(Forum forum);
	public List<Forum> getAllForumm();	
	public List<Forum> getAllForumByIdKategori(int id);
	public Forum getByIdForum(int id);
}
