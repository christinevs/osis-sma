package com.osis.services;

import java.util.List;

import com.osis.models.Forum;
import com.osis.models.Kategori;

public interface KategoriService {
	public Forum save(Kategori kategori);
	public List<Kategori> getAllKategori();
	public Kategori getById(int id);
}
